# Serverless CICD

## GitLab Environement Variable

[AWS Environment Variables](https://docs.gitlab.com/ee/ci/variables/#custom-variables-validated-by-gitlab) that should be set in GitLab to Deploy Serverless Application:

* `AWS_DEFAULT_REGION` *REQUIRED*
* `AWS_ACCESS_KEY_ID` *REQUIRED*
* `AWS_SECRET_ACCESS_KEY` *REQUIRED*

Swagger Environment Variables that should be set in GitLab to Publish OpenAPI 3.0 documentations.

* `SWAGGER_USER` *Optional, only to publish, OpenAPI 3.0 file will still be generated*
* `SWAGGER_API_KEY` *Optional, only to publish, OpenAPI 3.0 file will still be generated*
* `SWAGGER_PROJECT_NAME` *Optional, only to publish, OpenAPI 3.0 file will still be generated*

**_NOTES_**: Swagger documentation is public by default, but the OpenAPI 3.0 file can also be import in Insomnia and Postman or be read directly from [GitLab OpenAPI Viewer](https://docs.gitlab.com/ee/user/project/repository/#openapi-viewer). If these variables are not set, the CI mechanisms will not publish the documentation.

## `package.json` Scripts

To be execute only from GitLab CI:

### `yarn print`

Show the final result of serverless template and output information to be stored in Pipeline artifact `serverless_print_logs.txt`.

### `yarn deploy`

Deploy serverless application, Logs are stored in Pipeline artifact `serverless_deploy_logs.txt`.

### `yarn generate_doc`

Generate OpenAPI documentation and store the file to Pipeline artifact `openapi.json`.

### `yarn publish_doc`

Publish OpenAPI documentation to Swagger.
