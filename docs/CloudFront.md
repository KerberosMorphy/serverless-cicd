# Jobs CloudFront

Il s'agit simplement d'exécution de la fonction préalablement créé `invalidate_cloudfront`.

Ce script est en fonction de `vuejs:app:deploy` puisque c'est dans ce dernier que l'on définit l'ID de la distribution CloudFront à invalider.

```yml
cloudfront:invalidation:
  image: amazon/aws-cli:latest
  stage: invalidation
  script:
    - invalidate_cloudfront
  dependencies:
    - vuejs:app:deploy
  when: manual
```
