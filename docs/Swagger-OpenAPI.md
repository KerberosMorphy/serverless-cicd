# Jobs Swagger et OpenAPI

Il s'agit d'une démonstration de génération de documentation pour l'API Serverless ainsi que de la mise en ligne sur Swagger.

GitLab ne semble pas apprécier les noms des fichiers, ils ne peuvent donc pas être facilement visualisés dans GitLab.

## OpenAPI

Il s'agit ici de générer une documentation suivant les standards OpenAPI 3.0, pour se faire j'utilise le plugin `serverless-aws-documentation`. La documentation est par défaut consultable sur AWS Gateway, le script que j'ai créé l'extract grâce a aws-cli.

```yml
openapi:generate_doc:
  image: $SERVERLESS_IMAGE$SERVERLESS_DIR:latest
  stage: generate_doc
  script:
    - serverless_set_directory
    - ln -s /opt/app/node_modules ./node_modules
    - yarn generate_doc
  artifacts:
    paths:
      - openapi.json
    expire_in: 1 month
  only:
    changes:
      - $SERVERLESS_DIR/docs/*.{json,yml}
      - gitlab-ci/Jobs/OpenAPI-Generate.gitlab-ci.yml
  allow_failure: true
```

Le point principal est l'exécution de `yarn generate_doc` qui exécute: `chmod +x ./scripts/generate_openapi.sh && ./scripts/generate_openapi.sh` qui récupère l'ID de l'API pour ensuite généré le fichier.

```sh
apiId=`bash -c "aws apigateway get-rest-apis --output=json --region=$AWS_DEFAULT_REGION | /usr/bin/env node ./extract-rest-api-id.js $STAGE"`
outputFileNameOAS30=openapi.json

aws apigateway get-export \
  --rest-api-id=$apiId \
  --stage-name=$STAGE \
  --export-type=oas30 \
  --accept=application/$fileType \
  --region=$AWS_DEFAULT_REGION \
  $outputFileNameOAS30
```

J'utilise ensuite un script JS pour faire quelque modification au fichier.

```sh
bash -c "node ./rebuildJSON.js $CI_PROJECT_DIR/$outputFileNameOAS30"
```

En soi, je retirais des éléments superflux comme les appels `OPTION`.

```js
var jsonfile = require('jsonfile')
var file = process.argv[2]
jsonfile.readFile(file, function(err, obj) {
    if (err) {
        console.error(err);
    } else {
        var result = JSON.parse(JSON.stringify(obj, function(key, value) {
          if (key === 'options') {
            return undefined;
          }
          else if (key === 'default' && value[0] === '/') {
            return value.substring(1);
          }
          else if (key === 'title') {
            return process.env.CI_PROJECT_NAME;
          }
          return value
        }));
        jsonfile.writeFile(file, result, function(err) {
            err ? console.error(err) : console.log(`${file} exported successfully.`);
        })
    }
})
```

Le fichier est exporté dans les artifacts et peut ensuite être téléchargé pour utilisé avec Insomnia ou Postman, il pourrait aussi être commité et utilisé sur GitLab qui possède un outil similaire a Swagger pour tester des API REST.

## Swagger

Étape optionnelle, elle sert à publier notre documentation sur Swagger. Elle demande à configuration de token.

```yml
swagger:publish_doc:
  image: $SERVERLESS_IMAGE$SERVERLESS_DIR:latest
  stage: publish_doc
  script:
    - serverless_set_directory
    - ln -s /opt/app/node_modules ./node_modules
    - yarn publish_doc
  artifacts:
    paths:
      - openapi.json
    expire_in: 1 month
  dependencies:
    - openapi:generate_doc
  only:
    variables:
      - $SWAGGER_USER != null
      - $SWAGGER_API_KEY != null
  when: manual
  allow_failure: true
```

Un point intéressant à regarder est `only:variables`, je demande donc d'exécuter cette `Job` uniquement si mes variables d'environnement sont définies.

Le script exécuté sera `chmod +x ./scripts/publish_openapi.sh && ./scripts/publish_openapi.sh` qui est en soit un appel à l'api de Swagger pour modifier le projet déjà existant avec la nouvelle documentation.

```sh
docURL=https://app.swaggerhub.com/apis-docs/$SWAGGER_USER/$SWAGGER_PROJECT_NAME/$STAGE
filename=openapi.json

curl -X POST "https://api.swaggerhub.com/apis/$SWAGGER_USER/$SWAGGER_PROJECT_NAME?isPrivate=false&version=$STAGE&oas=3.0.1" \
    -H "accept: application/json" -H "Authorization: $SWAGGER_API_KEY" -H "Content-Type: application/json" \
    -d @$CI_PROJECT_DIR/$filename
```
